$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})


$(function() {
  $('#class_type').change(function(){
    $('.colors').hide();
    $('#' + $(this).val()).show();
  }); 

  $('#Payment_type').change(function(){
    $('.colors2').hide();
    $('#' + $(this).val()).show();
  });

  $('#billing_type').change(function(){
    $('.colors3').hide();
    $('#' + $(this).val()).show();
  });

  $('#payout_method_type').change(function(){
    $('.colors4').hide();
    $('#' + $(this).val()).show();
  }); 

  // $('#timesheet_method_type').change(function(){
    
  //   if ($(this).val() == 'revenue-share-opt1') { 
      
  //     $('#hours, #pay_rate, #payout').addClass('hidden-value');
  //     $('#hours, #pay_rate, #payout').removeClass('show-value');
  //     $('.rev-view-filled').addClass('show-value');

  //     $('.inner-hourly').addClass('hidden-value');
  //     $('.inner-hourly').removeClass('show-value');
  //     $('.inner-default').addClass('show-value');
  //   } 
  //   else {
  //     /*$('#hours, #pay_rate, #payout').addClass('show-value');
  //     $('#rev_share, #hourly').addClass('hidden-value');*/

  //     $('#hours, #pay_rate, #payout').addClass('show-value');
  //     $('.rev-view-filled').removeClass('show-value');

  //     $('.inner-hourly').addClass('show-value');
  //     $('.inner-default').addClass('hidden-value');
  //     $('.inner-default').removeClass('show-value');
  //   }
  // }); 


  $('#payout_method_type').change(function(){
    if($(this).val() == 'revenue-share-option'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
    
    }
    else if($(this).val() == 'hourly-option')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
    }
  });

  $('#Payment_type').change(function(){
    if($(this).val() == 'invoiced_account'){ 
        $( ".invoicedaccount" ).addClass( "facility-show" );
        $( ".parentpaid" ).removeClass( "facility-show" );
        $( "#halt_date" ).hide();
    }
    else if($(this).val() == 'parent_paid')
    {
        $( ".invoicedaccount" ).removeClass( "facility-show" );
        $( ".parentpaid" ).addClass( "facility-show" );
        $( "#halt_date" ).show();
    }
    else
    {
      $( ".parentpaid" ).removeClass( "facility-show" );
      $( "#halt_date" ).show(); 
    }
  });

  
  $(document).ready(function(){
    $(".linkround-blk").click(function(){
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });

    $(".cancel-both-popup").click(function(){
      $('#add-calendar-event').modal('hide');
    });

    $(".linkround-blk").click(function(){
     
      $("#"+$(this).attr("data-id")).addClass("click-change-icon");
    });
    

  });

});
/*
jQuery(document).ready(function(){
  jQuery('.scrollbar-macosx').scrollbar();
});
*/						
/* Ends of park class */

  
$(document).ready(function () {
  autoPlayYouTubeModal();
});

$('.multiple-select-date').datepicker({
  multidate: true,
  format: 'dd-mm-yyyy'
});

var array = ["2018-02-14","2018-01-15","2013-01-26"]
$('.redcolor-select-option').datepicker({
  multidate: true,
  datesDisabled: ['01/02/2019', '02/21/2019','01/14/2017']
  
});

/***  redcolor */

/*** end of redcolor */

function autoPlayYouTubeModal() {
      var trigger = $('.videoModalTriger');
  trigger.click(function () {
      var theModal = $(this).data("target");
      var videoSRC = $(this).attr("data-videoModal");
      var videoSRCauto = videoSRC + "?autoplay=1";
      $(theModal + ' iframe').attr('src', videoSRCauto);
      $(theModal).on('hidden.bs.modal', function (e) {
          $(theModal + ' iframe').attr('src', '');
      });
  });
};

$(document).ready(function(){  
  $(".sidebar-mobile-main-toggle").click(function(){
    //$('#navbar-mobile').hide();
    $('.nav-mobile').addClass('collapsed');
    $('.navbar-collapse.collapse').removeClass('in');
    
  });
  $(".open-unavailable-modal").click(function(){
    $("#updateevent-calendar-event").modal('show');
    $("#more-option-modal").modal('hide');
  });  
});

// .open-unavailable-modal




$(document).ready(function () {
  $('#passcheckbox1').change(function () {
    $('.pass-none').fadeToggle();
  });
});

$(document).ready(function() {
  $("#txtEditor").Editor();
});

$(document).ready(function() {
  $("#txtEditor2").Editor();
});

$(document).ready(function() {
  $("#txtEditor3").Editor();
    $(".Editor-editor").html("<p class='defalut_txt1' style='display:none;'>Please do not schedule me for an event during this time I am unavailable to work.</p>");
});



// $('#colorpalettediv').colorPalettePicker({
//   bootstrap: 3,
//   lines: 4
// });


/*
$(document).ready(function(){
  $("#navbar-mobile").click(function(){
    $(".sidebar-mobile-main .sidebar-div").hide();    
    $(".navbar-top").removeClass('sidebar-mobile-main');
  });
  $(".sidebar-mobile-main-toggle").click(function(){
    $("#navbar-mobile").hide();
  });

});
*/
/*
(function() {  
  var $point_arr, $points, $progress, $trigger, active, max, tracker, val;

  $trigger   = $('.trigger').first();
  $points    = $('.progress-points').first();
  $point_arr = $('.progress-point');
  $progress  = $('.progress').first();

  val     = +$points.data('current') - 1;
  max     = $point_arr.length - 1;
  tracker = active = 0;

  function activate(index) {
    if (index !== active) {
      active = index;
      var $_active = $point_arr.eq(active)
      
      $point_arr
        .removeClass('completed active')
        .slice(0, active).addClass('completed')
      
      $_active.addClass('active');
      
      return $progress.css('width', (index / max * 100) + "%");
    }
  };

  $points.on('click', 'li', function(event) {
    var _index;
    _index  = $point_arr.index(this);
    tracker = _index === 0 ? 1 : _index === val ? 0 : tracker;
    
    return activate(_index);
  });

  $trigger.on('click', function() {
    return activate(tracker++ % 2 === 0 ? 0 : val);
  });

  setTimeout((function() {
    return activate(val);
  }), 1000);

}).call(this);*/


/*
									
function editEvent(event) {
  $('#event-modal input[name="event-index"]').val(event ? event.id : '');
  $('#event-modal input[name="event-name"]').val(event ? event.name : '');
  $('#event-modal input[name="event-location"]').val(event ? event.location : '');
  $('#event-modal input[name="event-start-date"]').datepicker('update', event ? event.startDate : '');
  $('#event-modal input[name="event-end-date"]').datepicker('update', event ? event.endDate : '');
  $('#event-modal').modal();
}

function deleteEvent(event) {
  var dataSource = $('#calendar').data('calendar').getDataSource();

  for(var i in dataSource) {
    if(dataSource[i].id == event.id) {
      dataSource.splice(i, 1);
      break;
    }
  }
  
  $('#calendar').data('calendar').setDataSource(dataSource);
}

function saveEvent() {
  var event = {
    id: $('#event-modal input[name="event-index"]').val(),
    name: $('#event-modal input[name="event-name"]').val(),
    location: $('#event-modal input[name="event-location"]').val(),
    startDate: $('#event-modal input[name="event-start-date"]').datepicker('getDate'),
    endDate: $('#event-modal input[name="event-end-date"]').datepicker('getDate')
  }
  
  var dataSource = $('#calendar').data('calendar').getDataSource();

  if(event.id) {
    for(var i in dataSource) {
      if(dataSource[i].id == event.id) {
        dataSource[i].name = event.name;
        dataSource[i].location = event.location;
        dataSource[i].startDate = event.startDate;
        dataSource[i].endDate = event.endDate;
      }
    }
  }
  else
  {
    var newId = 0;
    for(var i in dataSource) {
      if(dataSource[i].id > newId) {
        newId = dataSource[i].id;
      }
    }
    
    newId++;
    event.id = newId;
  
    dataSource.push(event);
  }
  
  $('#calendar').data('calendar').setDataSource(dataSource);
  $('#event-modal').modal('hide');
}

$(function() {
  var currentYear = new Date().getFullYear();

  $('#calendar').calendar({ 
    enableContextMenu: true,
    enableRangeSelection: true,
    contextMenuItems:[
      {
        text: 'Update',
        click: editEvent
      },
      {
        text: 'Delete',
        click: deleteEvent
      }
    ],
    selectRange: function(e) {
      editEvent({ startDate: e.startDate, endDate: e.endDate });
    },
    mouseOnDay: function(e) {
      if(e.events.length > 0) {
        var content = '';
        
        for(var i in e.events) {
          content += '<div class="event-tooltip-content">'
                  + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                  + '<div class="event-location">' + e.events[i].location + '</div>'
                + '</div>';
        }
      
        $(e.element).popover({ 
          trigger: 'manual',
          container: 'body',
          html:true,
          content: content
        });
        
        $(e.element).popover('show');
      }
    },
    mouseOutDay: function(e) {
      if(e.events.length > 0) {
        $(e.element).popover('hide');
      }
    },
    dayContextMenu: function(e) {
      $(e.element).popover('hide');
    },
    dataSource: [
      {
        id: 0,
        name: 'Google I/O',
        location: 'San Francisco, CA',
        startDate: new Date(currentYear, 4, 28),
        endDate: new Date(currentYear, 4, 29)
      },
      {
        id: 1,
        name: 'Microsoft Convergence',
        location: 'New Orleans, LA',
        startDate: new Date(currentYear, 2, 16),
        endDate: new Date(currentYear, 2, 19)
      },
      {
        id: 2,
        name: 'Microsoft Build Developer Conference',
        location: 'San Francisco, CA',
        startDate: new Date(currentYear, 3, 29),
        endDate: new Date(currentYear, 4, 1)
      },
      {
        id: 3,
        name: 'Apple Special Event',
        location: 'San Francisco, CA',
        startDate: new Date(currentYear, 8, 1),
        endDate: new Date(currentYear, 8, 1)
      },
      {
        id: 4,
        name: 'Apple Keynote',
        location: 'San Francisco, CA',
        startDate: new Date(currentYear, 8, 9),
        endDate: new Date(currentYear, 8, 9)
      },
      {
        id: 5,
        name: 'Chrome Developer Summit',
        location: 'Mountain View, CA',
        startDate: new Date(currentYear, 10, 17),
        endDate: new Date(currentYear, 10, 18)
      },
      {
        id: 6,
        name: 'F8 2015',
        location: 'San Francisco, CA',
        startDate: new Date(currentYear, 2, 25),
        endDate: new Date(currentYear, 2, 26)
      },
      {
        id: 7,
        name: 'Yahoo Mobile Developer Conference',
        location: 'New York',
        startDate: new Date(currentYear, 7, 25),
        endDate: new Date(currentYear, 7, 26)
      },
      {
        id: 8,
        name: 'Android Developer Conference',
        location: 'Santa Clara, CA',
        startDate: new Date(currentYear, 11, 1),
        endDate: new Date(currentYear, 11, 4)
      },
      {
        id: 9,
        name: 'LA Tech Summit',
        location: 'Los Angeles, CA',
        startDate: new Date(currentYear, 10, 17),
        endDate: new Date(currentYear, 10, 17)
      }
    ]
  });
  
  $('#save-event').click(function() {
    saveEvent();
  });
}); */

